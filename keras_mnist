#coding:utf-8
'''
   keras实现lenet-5网络模型对mnist手写数字识别
'''
from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import Dense,Dropout,MaxPooling2D,Flatten
import numpy as np
np.random.seed(10)

#第一部分：准备数据
# 准备训练集和测试集数据
(x_train,y_train),(x_test,y_test)=mnist.load_data()
x_train4 = x_train.reshape(x_train.shape[0],28,28,1).astype('float32')
x_train4_norm = x_train4/255

x_test4 = x_test.reshape(x_test.shape[0],28,28,1).astype('float32')
x_test4_norm = x_test4/255

#准备标签数据，进行编码处理
y_train_1hot = np_utils.to_categorical(y_train,10)
y_test_1hot = np_utils.to_categorical(y_test,10)

#第二部分：构建网络模型
model = Sequential()
#1.第一个卷积层：输入图像为28x28x1，卷积核5x5，共6个卷积核，边界策略padding设置为same保持不变，激活函数采用relu
model.add(Conv2D(filters=6,
                 kernel_size=(5,5),
                 padding='same',
                 input_shape=(28,28,1),
                 activation='relu'))
#2.第一个池化层，大小为2x2
model.add(MaxPooling2D(pool_size=(2,2)))
#3.第二个卷积层：卷积核5x5，共16个卷积核，边界策略padding设置为same保持不变，激活函数采用relu
model.add(Conv2D(filters=16,
                 kernel_size=(5,5),
                 padding='same',
                 activation='relu'))
#4.第二个池化层，大小为2x2
model.add(MaxPooling2D(pool_size=(2,2)))

#5.采用dropout策略
model.add(Dropout(0.25))
#6.拉平特征体成一维向量
model.add(Flatten())
#7.搭建神经网络第一个隐层，120个节点，激活函数使用relu
model.add(Dense(120,activation='relu'))
#8.搭建神经网络第二个隐层，84个节点，激活函数使用relu
model.add(Dense(84,activation='relu'))
model.add(Dropout(0.25))
#9.搭建神经网络输出层，10个节点，激活函数采用softmax
model.add(Dense(10,activation='softmax'))

#第三部分：对网络模型进行编译
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
#第四部分：对网络模型进行训练
model.fit(x=x_train4_norm,
          y=y_train_1hot,
          validation_split=0.2,
          epochs=10,
          batch_size=300,verbose=2)

#第五部分：使用训练好的模型开展测试应用
print("预测结果：",model.predict_classes(x_test4_norm[:10]))
print("实际标签:",y_test[:10])