#encoding=utf-8
import io,sys
import requests,bs4,pyttsx3
from tkinter import *
import webbrowser
import time

sys.stdout=io.TextIOWrapper(sys.stdout.buffer,encoding='gb18030')
class Musk():
    def __init__(self):
        self.headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36 Edg/90.0.818.39',
            'referer': 'https://www.dogebank.com/'
        }
        self.url='https://www.dogebank.com/musk'

    def getTwiter(self):
        r = requests.get(self.url, headers=self.headers,verify=False).text
        requests.packages.urllib3.disable_warnings
        pubtime_divs = bs4.BeautifulSoup(r,'html.parser').select('div.messages_sender .name_bo')
        pubtimes = [item.text.strip() for item in pubtime_divs]
        pubmessages_divs = bs4.BeautifulSoup(r,'html.parser').select('div.messages_info')
        pubmessages = [item.text.strip() for item in pubmessages_divs]
        return pubtimes,pubmessages

def twiter_speak():
    musk = Musk()
    times,messages = musk.getTwiter()
    pyttsx3.speak("大家好，这里是稻谷编程，我是你们的好朋友小乐，下面我给您播放马斯克最新的推文信息。注意是英语哦")
    i=0
    for tlist,message in zip(times,messages):
        says = message+'; '+'发布时间为:'+tlist[10:]
        pyttsx3.speak("第{}条推文:".format(i+1))
        pyttsx3.speak(says)
        i+=1
        time.sleep(2)

#窗口视图呈现部分
class waUI(Tk):
    def __init__(self):
        super().__init__()
        self.geometry('500x400')
        self.title('马斯克推文小程序 -- 稻谷编程')
        self.resizable(0,0)
        self.photo = PhotoImage(file="e:/musk.gif")
        self.photo2 = PhotoImage(file="e:/voice.gif")
        self.widgets()

    def widgets(self):
        label = Label(self)
        label['font']=("华文行楷", 20)
        label['image']=self.photo
        label['text']=''
        label.pack()
        btn1 = Button(self)
        btn1['width']= 14
        btn1['height']= 2
        btn1['text']="浏览推文页面"
        btn1['font']=("黑体", 18)
        btn1['bg']='#f60'
        btn1['fg']='yellow'
        btn1['relief']='groove'
        btn1['command'] = self.btnAction
        btn1.place(x=50,y=310)
        btn2 = Button(self)
        btn2['width'] = 16
        btn2['height'] = 2
        btn2['text'] = "黑科技爬取播放"
        btn2['font'] = ("黑体", 18)
        btn2['bg'] = '#f60'
        btn2['fg'] = 'yellow'
        btn2['relief'] = 'groove'
        btn2['command'] = self.btnAction2
        btn2.place(x=240,y=310)


    def btnAction(self):
        webbrowser.open("https://www.dogebank.com/musk")

    def btnAction2(self):
        twiter_speak()

#运行主程序
if __name__=='__main__':
    app=waUI()
    app.mainloop()
